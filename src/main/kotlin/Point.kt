internal class Point(var x: Int, var y: Int) : IMovable {
    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }
}