internal class CloudOfPoints(val points: ArrayList<Point>): IMovable{
    override fun move(dx: Int, dy: Int) {
        for (i in points) {
            i.x += dx
            i.y += dy
        }
    }
}