# Реализация интерфейса Movable

Определены два класса: `Point` (точка с координатами `x`, `y`) и `CloudOfPoints` (набор точек `Point`).

```kotlin
internal class Point(var x:Int, var y:Int)
internal class CloudOfPoints(val points: ArrayList<Point>)
```

Дополните классы новой функциональностью - реализуйте интерфейс `Movable` (метод `move(dx, dy)`
добавляет `dx` к координате `x`, а `dy` к координате `y`). 
Необходимо написать фрагмент программы: полное описание классов с реализованным интерфейсом.

```kotlin
interface Movable {
    fun move( dx: Int, dy: Int)
}

// Ваш фрагмент будет помещён здесь

fun main() {
    val p = Point(1,1)
    val cloud = CloudOfPoints(arrayListOf(Point(1,1)))
    p.move(1,1); cloud.move(2,2)
    val p2 = cloud.points[0]
    println("${p.x} ${p.y}")
    println("${p2.x} ${p2.y}")
}
```